import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';

// Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
// import 'package:meta/meta.dart';
// import 'package:mockito/mockito.dart';

// Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
// import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/libraries/validate_objects_rules.dart';

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_value_failures.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/types/types.dart';

// Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
// class MockValidateObjectsRules extends Mock implements ValidateObjectsRules {}

class EmailAddressTests {
  EmailAddressTests(String testGroup, List<Map<String, dynamic>> testCases) {
    assert(testGroup != null);
    assert(testCases != null);

    if (testCases.length >= 1) {
      group(testGroup, () {
        // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
        // MockValidateObjectsRules mockValidateObjectsRules;

        // Factory constructor
        // ValidateObjects useCase;

        setUp(() {
          // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
          // final mockValidateObjectsRules = MockValidateObjectsRules();

          // Factory constructor
          // final useCase = ValidateObjects();
        });

        Map<String, dynamic> testCase;
        for (testCase in testCases) {
          tests(testCase);
        }
      });
    }
  }

  void tests(Map<String, dynamic> testCase) {
    test(testCase['test'], () async {
      // Arrange
      // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
      // when(mockValidateObjectsRules.validateEmailAddress(
      //         emailAddress: emailAddressValidEmail['emailAddress']))
      //     .thenAnswer((_) => right(emailAddressValidEmail['emailAddress']));
      //     OR .thenAnswer((_) => emailAddressValidEmail['result']);

      // Act
      final EmailAddress useCase =
          EmailAddress(emailAddressString: testCase['emailAddress']);
      final Either<AbstractValueFailures<String>, String> actual =
          useCase.value;

      // Assert
      expect(actual, testCase['matcher']);

      // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
      // verify(mockValidateObjectsRules.validateEmailAddress(
      //     emailAddress: emailAddressValidEmail['emailAddress']));
      // verifyNoMoreInteractions(mockValidateObjectsRules);
    });
  }
}

class PasswordTests {
  PasswordTests(String testGroup, List<Map<String, dynamic>> testCases) {
    assert(testGroup != null);
    assert(testCases != null);

    if (testCases.length >= 1) {
      group(testGroup, () {
        // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
        // MockValidateObjectsRules mockValidateObjectsRules;

        // Factory constructor
        // ValidateObjects useCase;

        setUp(() {
          // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
          // final mockValidateObjectsRules = MockValidateObjectsRules();

          // Factory constructor
          // final useCase = ValidateObjects();
        });

        Map<String, dynamic> testCase;
        for (testCase in testCases) {
          tests(testCase);
        }
      });
    }
  }

  void tests(Map<String, dynamic> testCase) {
    test(testCase['test'], () async {
      // Arrange
      // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
      // when(mockValidateObjectsRules.validateEmailAddress(
      //         emailAddress: emailAddressValidEmail['emailAddress']))
      //     .thenAnswer((_) => right(emailAddressValidEmail['emailAddress']));
      //     OR .thenAnswer((_) => emailAddressValidEmail['result']);

      // Act
      final Password useCase = Password(passwordString: testCase['password']);
      final Either<AbstractValueFailures<String>, String> actual =
          useCase.value;

      // Assert
      expect(actual, testCase['matcher']);

      // Mockito does not support =static methods: The method ‘{0}’ isn’t defined for the type ‘{1}’.
      // verify(mockValidateObjectsRules.validateEmailAddress(
      //     emailAddress: emailAddressValidEmail['emailAddress']));
      // verifyNoMoreInteractions(mockValidateObjectsRules);
    });
  }
}

void main() {
  String testGroup;
  // Must initialise the List to an empty list otherwise the .add fails
  List<Map<String, dynamic>> testCases;

  // * Email Address Tests

  String emailAddressString;

  testGroup = 'Constructor: EmailAddress (also tests ValidateObjects)';
  testCases = [];

  emailAddressString = 'abc@def.com';
  testCases.add({
    'test': 'Valid: Proper Email Address',
    'emailAddress': emailAddressString,
    'matcher': right(emailAddressString)
  });

  emailAddressString = 'abc';
  testCases.add({
    'test': 'Invalid: String As Email Address',
    'emailAddress': emailAddressString,
    'matcher': left(AbstractValueFailures<String>.invalidEmail(
        failedValue: emailAddressString))
  });

  emailAddressString = 'abc@def';
  testCases.add({
    'test': 'Invalid: Email Address Without Domain',
    'emailAddress': emailAddressString,
    'matcher': left(AbstractValueFailures<String>.invalidEmail(
        failedValue: emailAddressString))
  });

  // TODO: Add additional email Address tests

  EmailAddressTests(testGroup, testCases);

  // * Password Tests

  String passwordString;

  testGroup = 'Constructor: Password (also tests ValidateObjects)';
  testCases = [];

  passwordString = '1234567890';
  testCases.add({
    'test': 'Valid: ${passwordString.length} Bytes Long',
    'password': passwordString,
    'matcher': right(passwordString)
  });

  passwordString = '12345678901';
  testCases.add({
    'test': 'Valid: ${passwordString.length} Bytes Long',
    'password': passwordString,
    'matcher': right(passwordString)
  });

  passwordString =
      '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
  testCases.add({
    'test': 'Valid: ${passwordString.length} Bytes Long',
    'password': passwordString,
    'matcher': right(passwordString)
  });

  passwordString = '123456789';
  testCases.add({
    'test': 'Invalid: ${passwordString.length} Bytes Long',
    'password': passwordString,
    'matcher': left(
        AbstractValueFailures<String>.weakPassword(failedValue: passwordString))
  });

  passwordString = '1';
  testCases.add({
    'test': 'Invalid: ${passwordString.length} Byte Long',
    'password': passwordString,
    'matcher': left(
        AbstractValueFailures<String>.weakPassword(failedValue: passwordString))
  });

  // TODO: Add additional password tests

  PasswordTests(testGroup, testCases);
}
