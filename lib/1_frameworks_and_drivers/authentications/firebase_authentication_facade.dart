import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_authentication_failures.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/interfaces/interface_authentication_facade.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/types/types.dart';

class FirebaseAuthenticationFacade implements InterfaceAuthenticationFacade {
  final FirebaseAuth fireBaseAuth;
  final GoogleSignIn googleSignIn;

  FirebaseAuthenticationFacade(
      {@required this.fireBaseAuth, @required this.googleSignIn});

  @override
  Future<Either<AbstractAuthenticationFailures, Unit>>
      registerWithEmailAndPassword(
          {@required EmailAddress emailAddress,
          @required Password password}) async {
    final String emailAddressString = emailAddress.fold();
    final String passwordString = password.fold();

    try {
      await fireBaseAuth.createUserWithEmailAndPassword(
          email: emailAddressString, password: passwordString);

      return right(unit);
    } on PlatformException catch (error) {
      if (error.code == 'ERROR_EMAIL_ALREADY_IN_USE') {
        return left(const AbstractAuthenticationFailures
            .emailAddressAlreadyRegistered());
      } else {
        // This covers errors 'ERROR_INVALID_EMAIL' and 'ERROR_WEAK_PASSWORD'
        return left(const AbstractAuthenticationFailures.generalError());
      }
    }
  }

  @override
  Future<Either<AbstractAuthenticationFailures, Unit>>
      loginWithEmailAndPassword(
          {@required EmailAddress emailAddress,
          @required Password password}) async {
    final String emailAddressString = emailAddress.fold();
    final String passwordString = password.fold();

    try {
      await fireBaseAuth.signInWithEmailAndPassword(
          email: emailAddressString, password: passwordString);

      return right(unit);
    } on PlatformException catch (error) {
      if (error.code == 'ERROR_INVALID_EMAIL' ||
          error.code == 'ERROR_WRONG_PASSWORD') {
        return left(const AbstractAuthenticationFailures
            .invalidEmailAddressOrPassword());
      } else {
        // This covers errors 'ERROR_USER_NOT_FOUND', 'ERROR_USER_DISABLED', 'ERROR_TOO_MANY_REQUESTS' and 'ERROR_OPERATION_NOT_ALLOWED'
        return left(const AbstractAuthenticationFailures.generalError());
      }
    }
  }

  @override
  Future<Either<AbstractAuthenticationFailures, Unit>> loginWithGoogle() async {
    try {
      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();

      if (googleSignInAccount == null) {
        return left(AbstractAuthenticationFailures.loginCancelled());
      }

      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential authCredential = GoogleAuthProvider.getCredential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);

      await fireBaseAuth.signInWithCredential(authCredential);

      return right(unit);
    } on PlatformException catch (_) {
      return left(const AbstractAuthenticationFailures.generalError());
    }
  }
}
