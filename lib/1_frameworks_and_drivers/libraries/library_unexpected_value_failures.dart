library library_unexpected_value_failures.dart;

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_value_failures.dart';

class LibraryUnexpectedValueFailures extends Error {
  final AbstractValueFailures abstractValueFailures;

  LibraryUnexpectedValueFailures(this.abstractValueFailures);

  @override
  String toString() {
    return Error.safeToString(
        'Unexpected Value Failures! Failure: $abstractValueFailures');
  }
}
