import 'package:freezed_annotation/freezed_annotation.dart';

// Must run: flutter pub run build_runner watch
part 'abstract_value_failures.freezed.dart';

@immutable
@freezed
abstract class AbstractValueFailures<T> with _$AbstractValueFailures<T> {
  const factory AbstractValueFailures.invalidEmail(
      {@required String failedValue}) = InvalidEmail<T>;

  const factory AbstractValueFailures.weakPassword(
      {@required String failedValue}) = WeakPassword<T>;
}
