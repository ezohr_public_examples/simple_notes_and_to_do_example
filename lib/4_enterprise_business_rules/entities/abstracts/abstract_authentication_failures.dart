import 'package:freezed_annotation/freezed_annotation.dart';

// Must run: flutter pub run build_runner watch
part 'abstract_authentication_failures.freezed.dart';

@immutable
@freezed
abstract class AbstractAuthenticationFailures<T>
    with _$AbstractAuthenticationFailures<T> {
  // * Possible errors: Any not specifically caught errors on authentication server
  const factory AbstractAuthenticationFailures.generalError() =
      GeneralServerError;

  // * Possible errors: Attempt to register with already registered email address
  const factory AbstractAuthenticationFailures.emailAddressAlreadyRegistered() =
      EmailAddressAlreadyRegistered;

  // * Possible errors: Invalid email address or password for login
  const factory AbstractAuthenticationFailures.invalidEmailAddressOrPassword() =
      InvalidEmailAddressOrPassword;

  // * Possible errors: Login cancelled (Google)
  const factory AbstractAuthenticationFailures.loginCancelled() =
      LoginCancelled;
}
