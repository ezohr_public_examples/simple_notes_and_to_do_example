import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

import 'package:simple_notes_and_to_do_example/1_frameworks_and_drivers/libraries/library_unexpected_value_failures.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_value_failures.dart';

@immutable
abstract class AbstractValueObject<T> {
  const AbstractValueObject();

  Either<AbstractValueFailures<T>, T> get value;

  bool isValid() => value.isRight();

  T fold() {
    // id becomes the equivalent of (right) => right);
    return value.fold(
        (failure) => throw LibraryUnexpectedValueFailures(failure), id);
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is AbstractValueObject<T> && o.value == value;
  }

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'Value($value)';
}
