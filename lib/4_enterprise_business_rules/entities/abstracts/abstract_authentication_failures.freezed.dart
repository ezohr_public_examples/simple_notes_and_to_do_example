// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'abstract_authentication_failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$AbstractAuthenticationFailuresTearOff {
  const _$AbstractAuthenticationFailuresTearOff();

// ignore: unused_element
  GeneralServerError<T> generalError<T>() {
    return GeneralServerError<T>();
  }

// ignore: unused_element
  EmailAddressAlreadyRegistered<T> emailAddressAlreadyRegistered<T>() {
    return EmailAddressAlreadyRegistered<T>();
  }

// ignore: unused_element
  InvalidEmailAddressOrPassword<T> invalidEmailAddressOrPassword<T>() {
    return InvalidEmailAddressOrPassword<T>();
  }

// ignore: unused_element
  LoginCancelled<T> loginCancelled<T>() {
    return LoginCancelled<T>();
  }
}

// ignore: unused_element
const $AbstractAuthenticationFailures =
    _$AbstractAuthenticationFailuresTearOff();

mixin _$AbstractAuthenticationFailures<T> {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result generalError(),
    @required Result emailAddressAlreadyRegistered(),
    @required Result invalidEmailAddressOrPassword(),
    @required Result loginCancelled(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result generalError(),
    Result emailAddressAlreadyRegistered(),
    Result invalidEmailAddressOrPassword(),
    Result loginCancelled(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result generalError(GeneralServerError<T> value),
    @required
        Result emailAddressAlreadyRegistered(
            EmailAddressAlreadyRegistered<T> value),
    @required
        Result invalidEmailAddressOrPassword(
            InvalidEmailAddressOrPassword<T> value),
    @required Result loginCancelled(LoginCancelled<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result generalError(GeneralServerError<T> value),
    Result emailAddressAlreadyRegistered(
        EmailAddressAlreadyRegistered<T> value),
    Result invalidEmailAddressOrPassword(
        InvalidEmailAddressOrPassword<T> value),
    Result loginCancelled(LoginCancelled<T> value),
    @required Result orElse(),
  });
}

abstract class $AbstractAuthenticationFailuresCopyWith<T, $Res> {
  factory $AbstractAuthenticationFailuresCopyWith(
          AbstractAuthenticationFailures<T> value,
          $Res Function(AbstractAuthenticationFailures<T>) then) =
      _$AbstractAuthenticationFailuresCopyWithImpl<T, $Res>;
}

class _$AbstractAuthenticationFailuresCopyWithImpl<T, $Res>
    implements $AbstractAuthenticationFailuresCopyWith<T, $Res> {
  _$AbstractAuthenticationFailuresCopyWithImpl(this._value, this._then);

  final AbstractAuthenticationFailures<T> _value;
  // ignore: unused_field
  final $Res Function(AbstractAuthenticationFailures<T>) _then;
}

abstract class $GeneralServerErrorCopyWith<T, $Res> {
  factory $GeneralServerErrorCopyWith(GeneralServerError<T> value,
          $Res Function(GeneralServerError<T>) then) =
      _$GeneralServerErrorCopyWithImpl<T, $Res>;
}

class _$GeneralServerErrorCopyWithImpl<T, $Res>
    extends _$AbstractAuthenticationFailuresCopyWithImpl<T, $Res>
    implements $GeneralServerErrorCopyWith<T, $Res> {
  _$GeneralServerErrorCopyWithImpl(
      GeneralServerError<T> _value, $Res Function(GeneralServerError<T>) _then)
      : super(_value, (v) => _then(v as GeneralServerError<T>));

  @override
  GeneralServerError<T> get _value => super._value as GeneralServerError<T>;
}

class _$GeneralServerError<T> implements GeneralServerError<T> {
  const _$GeneralServerError();

  @override
  String toString() {
    return 'AbstractAuthenticationFailures<$T>.generalError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is GeneralServerError<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result generalError(),
    @required Result emailAddressAlreadyRegistered(),
    @required Result invalidEmailAddressOrPassword(),
    @required Result loginCancelled(),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return generalError();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result generalError(),
    Result emailAddressAlreadyRegistered(),
    Result invalidEmailAddressOrPassword(),
    Result loginCancelled(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (generalError != null) {
      return generalError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result generalError(GeneralServerError<T> value),
    @required
        Result emailAddressAlreadyRegistered(
            EmailAddressAlreadyRegistered<T> value),
    @required
        Result invalidEmailAddressOrPassword(
            InvalidEmailAddressOrPassword<T> value),
    @required Result loginCancelled(LoginCancelled<T> value),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return generalError(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result generalError(GeneralServerError<T> value),
    Result emailAddressAlreadyRegistered(
        EmailAddressAlreadyRegistered<T> value),
    Result invalidEmailAddressOrPassword(
        InvalidEmailAddressOrPassword<T> value),
    Result loginCancelled(LoginCancelled<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (generalError != null) {
      return generalError(this);
    }
    return orElse();
  }
}

abstract class GeneralServerError<T>
    implements AbstractAuthenticationFailures<T> {
  const factory GeneralServerError() = _$GeneralServerError<T>;
}

abstract class $EmailAddressAlreadyRegisteredCopyWith<T, $Res> {
  factory $EmailAddressAlreadyRegisteredCopyWith(
          EmailAddressAlreadyRegistered<T> value,
          $Res Function(EmailAddressAlreadyRegistered<T>) then) =
      _$EmailAddressAlreadyRegisteredCopyWithImpl<T, $Res>;
}

class _$EmailAddressAlreadyRegisteredCopyWithImpl<T, $Res>
    extends _$AbstractAuthenticationFailuresCopyWithImpl<T, $Res>
    implements $EmailAddressAlreadyRegisteredCopyWith<T, $Res> {
  _$EmailAddressAlreadyRegisteredCopyWithImpl(
      EmailAddressAlreadyRegistered<T> _value,
      $Res Function(EmailAddressAlreadyRegistered<T>) _then)
      : super(_value, (v) => _then(v as EmailAddressAlreadyRegistered<T>));

  @override
  EmailAddressAlreadyRegistered<T> get _value =>
      super._value as EmailAddressAlreadyRegistered<T>;
}

class _$EmailAddressAlreadyRegistered<T>
    implements EmailAddressAlreadyRegistered<T> {
  const _$EmailAddressAlreadyRegistered();

  @override
  String toString() {
    return 'AbstractAuthenticationFailures<$T>.emailAddressAlreadyRegistered()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EmailAddressAlreadyRegistered<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result generalError(),
    @required Result emailAddressAlreadyRegistered(),
    @required Result invalidEmailAddressOrPassword(),
    @required Result loginCancelled(),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return emailAddressAlreadyRegistered();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result generalError(),
    Result emailAddressAlreadyRegistered(),
    Result invalidEmailAddressOrPassword(),
    Result loginCancelled(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (emailAddressAlreadyRegistered != null) {
      return emailAddressAlreadyRegistered();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result generalError(GeneralServerError<T> value),
    @required
        Result emailAddressAlreadyRegistered(
            EmailAddressAlreadyRegistered<T> value),
    @required
        Result invalidEmailAddressOrPassword(
            InvalidEmailAddressOrPassword<T> value),
    @required Result loginCancelled(LoginCancelled<T> value),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return emailAddressAlreadyRegistered(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result generalError(GeneralServerError<T> value),
    Result emailAddressAlreadyRegistered(
        EmailAddressAlreadyRegistered<T> value),
    Result invalidEmailAddressOrPassword(
        InvalidEmailAddressOrPassword<T> value),
    Result loginCancelled(LoginCancelled<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (emailAddressAlreadyRegistered != null) {
      return emailAddressAlreadyRegistered(this);
    }
    return orElse();
  }
}

abstract class EmailAddressAlreadyRegistered<T>
    implements AbstractAuthenticationFailures<T> {
  const factory EmailAddressAlreadyRegistered() =
      _$EmailAddressAlreadyRegistered<T>;
}

abstract class $InvalidEmailAddressOrPasswordCopyWith<T, $Res> {
  factory $InvalidEmailAddressOrPasswordCopyWith(
          InvalidEmailAddressOrPassword<T> value,
          $Res Function(InvalidEmailAddressOrPassword<T>) then) =
      _$InvalidEmailAddressOrPasswordCopyWithImpl<T, $Res>;
}

class _$InvalidEmailAddressOrPasswordCopyWithImpl<T, $Res>
    extends _$AbstractAuthenticationFailuresCopyWithImpl<T, $Res>
    implements $InvalidEmailAddressOrPasswordCopyWith<T, $Res> {
  _$InvalidEmailAddressOrPasswordCopyWithImpl(
      InvalidEmailAddressOrPassword<T> _value,
      $Res Function(InvalidEmailAddressOrPassword<T>) _then)
      : super(_value, (v) => _then(v as InvalidEmailAddressOrPassword<T>));

  @override
  InvalidEmailAddressOrPassword<T> get _value =>
      super._value as InvalidEmailAddressOrPassword<T>;
}

class _$InvalidEmailAddressOrPassword<T>
    implements InvalidEmailAddressOrPassword<T> {
  const _$InvalidEmailAddressOrPassword();

  @override
  String toString() {
    return 'AbstractAuthenticationFailures<$T>.invalidEmailAddressOrPassword()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is InvalidEmailAddressOrPassword<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result generalError(),
    @required Result emailAddressAlreadyRegistered(),
    @required Result invalidEmailAddressOrPassword(),
    @required Result loginCancelled(),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return invalidEmailAddressOrPassword();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result generalError(),
    Result emailAddressAlreadyRegistered(),
    Result invalidEmailAddressOrPassword(),
    Result loginCancelled(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmailAddressOrPassword != null) {
      return invalidEmailAddressOrPassword();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result generalError(GeneralServerError<T> value),
    @required
        Result emailAddressAlreadyRegistered(
            EmailAddressAlreadyRegistered<T> value),
    @required
        Result invalidEmailAddressOrPassword(
            InvalidEmailAddressOrPassword<T> value),
    @required Result loginCancelled(LoginCancelled<T> value),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return invalidEmailAddressOrPassword(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result generalError(GeneralServerError<T> value),
    Result emailAddressAlreadyRegistered(
        EmailAddressAlreadyRegistered<T> value),
    Result invalidEmailAddressOrPassword(
        InvalidEmailAddressOrPassword<T> value),
    Result loginCancelled(LoginCancelled<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmailAddressOrPassword != null) {
      return invalidEmailAddressOrPassword(this);
    }
    return orElse();
  }
}

abstract class InvalidEmailAddressOrPassword<T>
    implements AbstractAuthenticationFailures<T> {
  const factory InvalidEmailAddressOrPassword() =
      _$InvalidEmailAddressOrPassword<T>;
}

abstract class $LoginCancelledCopyWith<T, $Res> {
  factory $LoginCancelledCopyWith(
          LoginCancelled<T> value, $Res Function(LoginCancelled<T>) then) =
      _$LoginCancelledCopyWithImpl<T, $Res>;
}

class _$LoginCancelledCopyWithImpl<T, $Res>
    extends _$AbstractAuthenticationFailuresCopyWithImpl<T, $Res>
    implements $LoginCancelledCopyWith<T, $Res> {
  _$LoginCancelledCopyWithImpl(
      LoginCancelled<T> _value, $Res Function(LoginCancelled<T>) _then)
      : super(_value, (v) => _then(v as LoginCancelled<T>));

  @override
  LoginCancelled<T> get _value => super._value as LoginCancelled<T>;
}

class _$LoginCancelled<T> implements LoginCancelled<T> {
  const _$LoginCancelled();

  @override
  String toString() {
    return 'AbstractAuthenticationFailures<$T>.loginCancelled()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoginCancelled<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result generalError(),
    @required Result emailAddressAlreadyRegistered(),
    @required Result invalidEmailAddressOrPassword(),
    @required Result loginCancelled(),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return loginCancelled();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result generalError(),
    Result emailAddressAlreadyRegistered(),
    Result invalidEmailAddressOrPassword(),
    Result loginCancelled(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginCancelled != null) {
      return loginCancelled();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result generalError(GeneralServerError<T> value),
    @required
        Result emailAddressAlreadyRegistered(
            EmailAddressAlreadyRegistered<T> value),
    @required
        Result invalidEmailAddressOrPassword(
            InvalidEmailAddressOrPassword<T> value),
    @required Result loginCancelled(LoginCancelled<T> value),
  }) {
    assert(generalError != null);
    assert(emailAddressAlreadyRegistered != null);
    assert(invalidEmailAddressOrPassword != null);
    assert(loginCancelled != null);
    return loginCancelled(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result generalError(GeneralServerError<T> value),
    Result emailAddressAlreadyRegistered(
        EmailAddressAlreadyRegistered<T> value),
    Result invalidEmailAddressOrPassword(
        InvalidEmailAddressOrPassword<T> value),
    Result loginCancelled(LoginCancelled<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginCancelled != null) {
      return loginCancelled(this);
    }
    return orElse();
  }
}

abstract class LoginCancelled<T> implements AbstractAuthenticationFailures<T> {
  const factory LoginCancelled() = _$LoginCancelled<T>;
}
