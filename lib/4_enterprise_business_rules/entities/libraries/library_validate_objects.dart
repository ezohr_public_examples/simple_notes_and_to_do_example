library library_validate_objects.dart;

import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_value_failures.dart';
// Youtube Course: Flutter And Domain Driven Design Course
// import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/libraries/value_objects.dart';

@immutable
class LibraryValidateObjects {
  // TODO: Must check Regular Expression rules
  // ValidateObjects can't be an instance member: Instance members can’t be accessed from a factory constructor.
  static Either<AbstractValueFailures<String>, String> validateEmailAddress(
      {@required String emailAddressString}) {
    // Youtube Course: Flutter And Domain Driven Design Course
    // Website: https://resocoder.com/2020/03/13/flutter-firebase-ddd-course-2-authentication-value-objects/#t-1595690768131
    // const emailRegex = r"""^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+""";

    // Website: https://emailregex.com
    const emailRegularExpression =
        r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""";

    if (RegExp(emailRegularExpression).hasMatch(emailAddressString)) {
      return right(emailAddressString);
    } else {
      return left(
          AbstractValueFailures.invalidEmail(failedValue: emailAddressString));
    }
  }

  // TODO: Must check minimum password length
  // TODO: Must add password complexity check
  // ValidateObjects can't be an instance member: Instance members can’t be accessed from a factory constructor.
  static Either<AbstractValueFailures<String>, String> validatePassword(
      {@required String passwordString}) {
    if (passwordString.length >= 10) {
      return right(passwordString);
    } else {
      return left(
          AbstractValueFailures.weakPassword(failedValue: passwordString));
    }
  }
}
