import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_authentication_failures.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/types/types.dart';

@immutable
abstract class InterfaceAuthenticationFacade {
  Future<Either<AbstractAuthenticationFailures, Unit>>
      registerWithEmailAndPassword(
          {@required EmailAddress emailAddress, @required Password password});

  Future<Either<AbstractAuthenticationFailures, Unit>>
      loginWithEmailAndPassword(
          {@required EmailAddress emailAddress, @required Password password});

  Future<Either<AbstractAuthenticationFailures, Unit>> loginWithGoogle();
}
