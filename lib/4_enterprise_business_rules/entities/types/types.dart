import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_value_failures.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_value_object.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/libraries/library_validate_objects.dart';

class EmailAddress extends AbstractValueObject<String> {
  final Either<AbstractValueFailures<String>, String> value;

  factory EmailAddress({@required String emailAddressString}) {
    assert(emailAddressString != null);

    // ValidateObjects can't be an instance member: Instance members can’t be accessed from a factory constructor.
    return EmailAddress._(LibraryValidateObjects.validateEmailAddress(
        emailAddressString: emailAddressString));
  }

  const EmailAddress._(this.value);
}

class Password extends AbstractValueObject<String> {
  final Either<AbstractValueFailures<String>, String> value;

  // ValidateObjects can't be an instance member: Instance members can’t be accessed from a factory constructor.
  factory Password({@required String passwordString}) {
    assert(passwordString != null);

    return Password._(LibraryValidateObjects.validatePassword(
        passwordString: passwordString));
  }

  const Password._(this.value);
}
