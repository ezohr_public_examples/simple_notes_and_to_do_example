part of 'login_form_bloc.dart';

// @immutable
// abstract class LoginFormEvent {}

// * Direction: From user interface
@immutable
@freezed
abstract class LoginFormEvent with _$LoginFormEvent {
  const factory LoginFormEvent.emailAddressChanged(
      {@required String emailAddressString}) = EmailAddressChanged;

  const factory LoginFormEvent.passwordChanged(
      {@required String passwordString}) = PasswordChanged;

  const factory LoginFormEvent.registerWithEmailAndPasswordPressed() =
      RegisterWithEmailAndPasswordPressed;

  const factory LoginFormEvent.loginWithEmailAndPasswordPressed() =
      LoginWithEmailAndPasswordPressed;

  const factory LoginFormEvent.loginWithGooglePressed() =
      LoginWithGooglePressed;
}
