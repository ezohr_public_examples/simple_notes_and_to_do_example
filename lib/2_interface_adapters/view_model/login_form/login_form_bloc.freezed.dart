// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'login_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$LoginFormEventTearOff {
  const _$LoginFormEventTearOff();

// ignore: unused_element
  EmailAddressChanged emailAddressChanged(
      {@required String emailAddressString}) {
    return EmailAddressChanged(
      emailAddressString: emailAddressString,
    );
  }

// ignore: unused_element
  PasswordChanged passwordChanged({@required String passwordString}) {
    return PasswordChanged(
      passwordString: passwordString,
    );
  }

// ignore: unused_element
  RegisterWithEmailAndPasswordPressed registerWithEmailAndPasswordPressed() {
    return const RegisterWithEmailAndPasswordPressed();
  }

// ignore: unused_element
  LoginWithEmailAndPasswordPressed loginWithEmailAndPasswordPressed() {
    return const LoginWithEmailAndPasswordPressed();
  }

// ignore: unused_element
  LoginWithGooglePressed loginWithGooglePressed() {
    return const LoginWithGooglePressed();
  }
}

// ignore: unused_element
const $LoginFormEvent = _$LoginFormEventTearOff();

mixin _$LoginFormEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result emailAddressChanged(String emailAddressString),
    @required Result passwordChanged(String passwordString),
    @required Result registerWithEmailAndPasswordPressed(),
    @required Result loginWithEmailAndPasswordPressed(),
    @required Result loginWithGooglePressed(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result emailAddressChanged(String emailAddressString),
    Result passwordChanged(String passwordString),
    Result registerWithEmailAndPasswordPressed(),
    Result loginWithEmailAndPasswordPressed(),
    Result loginWithGooglePressed(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result emailAddressChanged(EmailAddressChanged value),
    @required Result passwordChanged(PasswordChanged value),
    @required
        Result registerWithEmailAndPasswordPressed(
            RegisterWithEmailAndPasswordPressed value),
    @required
        Result loginWithEmailAndPasswordPressed(
            LoginWithEmailAndPasswordPressed value),
    @required Result loginWithGooglePressed(LoginWithGooglePressed value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result emailAddressChanged(EmailAddressChanged value),
    Result passwordChanged(PasswordChanged value),
    Result registerWithEmailAndPasswordPressed(
        RegisterWithEmailAndPasswordPressed value),
    Result loginWithEmailAndPasswordPressed(
        LoginWithEmailAndPasswordPressed value),
    Result loginWithGooglePressed(LoginWithGooglePressed value),
    @required Result orElse(),
  });
}

abstract class $LoginFormEventCopyWith<$Res> {
  factory $LoginFormEventCopyWith(
          LoginFormEvent value, $Res Function(LoginFormEvent) then) =
      _$LoginFormEventCopyWithImpl<$Res>;
}

class _$LoginFormEventCopyWithImpl<$Res>
    implements $LoginFormEventCopyWith<$Res> {
  _$LoginFormEventCopyWithImpl(this._value, this._then);

  final LoginFormEvent _value;
  // ignore: unused_field
  final $Res Function(LoginFormEvent) _then;
}

abstract class $EmailAddressChangedCopyWith<$Res> {
  factory $EmailAddressChangedCopyWith(
          EmailAddressChanged value, $Res Function(EmailAddressChanged) then) =
      _$EmailAddressChangedCopyWithImpl<$Res>;
  $Res call({String emailAddressString});
}

class _$EmailAddressChangedCopyWithImpl<$Res>
    extends _$LoginFormEventCopyWithImpl<$Res>
    implements $EmailAddressChangedCopyWith<$Res> {
  _$EmailAddressChangedCopyWithImpl(
      EmailAddressChanged _value, $Res Function(EmailAddressChanged) _then)
      : super(_value, (v) => _then(v as EmailAddressChanged));

  @override
  EmailAddressChanged get _value => super._value as EmailAddressChanged;

  @override
  $Res call({
    Object emailAddressString = freezed,
  }) {
    return _then(EmailAddressChanged(
      emailAddressString: emailAddressString == freezed
          ? _value.emailAddressString
          : emailAddressString as String,
    ));
  }
}

class _$EmailAddressChanged implements EmailAddressChanged {
  const _$EmailAddressChanged({@required this.emailAddressString})
      : assert(emailAddressString != null);

  @override
  final String emailAddressString;

  @override
  String toString() {
    return 'LoginFormEvent.emailAddressChanged(emailAddressString: $emailAddressString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EmailAddressChanged &&
            (identical(other.emailAddressString, emailAddressString) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddressString, emailAddressString)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(emailAddressString);

  @override
  $EmailAddressChangedCopyWith<EmailAddressChanged> get copyWith =>
      _$EmailAddressChangedCopyWithImpl<EmailAddressChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result emailAddressChanged(String emailAddressString),
    @required Result passwordChanged(String passwordString),
    @required Result registerWithEmailAndPasswordPressed(),
    @required Result loginWithEmailAndPasswordPressed(),
    @required Result loginWithGooglePressed(),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return emailAddressChanged(emailAddressString);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result emailAddressChanged(String emailAddressString),
    Result passwordChanged(String passwordString),
    Result registerWithEmailAndPasswordPressed(),
    Result loginWithEmailAndPasswordPressed(),
    Result loginWithGooglePressed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (emailAddressChanged != null) {
      return emailAddressChanged(emailAddressString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result emailAddressChanged(EmailAddressChanged value),
    @required Result passwordChanged(PasswordChanged value),
    @required
        Result registerWithEmailAndPasswordPressed(
            RegisterWithEmailAndPasswordPressed value),
    @required
        Result loginWithEmailAndPasswordPressed(
            LoginWithEmailAndPasswordPressed value),
    @required Result loginWithGooglePressed(LoginWithGooglePressed value),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return emailAddressChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result emailAddressChanged(EmailAddressChanged value),
    Result passwordChanged(PasswordChanged value),
    Result registerWithEmailAndPasswordPressed(
        RegisterWithEmailAndPasswordPressed value),
    Result loginWithEmailAndPasswordPressed(
        LoginWithEmailAndPasswordPressed value),
    Result loginWithGooglePressed(LoginWithGooglePressed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (emailAddressChanged != null) {
      return emailAddressChanged(this);
    }
    return orElse();
  }
}

abstract class EmailAddressChanged implements LoginFormEvent {
  const factory EmailAddressChanged({@required String emailAddressString}) =
      _$EmailAddressChanged;

  String get emailAddressString;
  $EmailAddressChangedCopyWith<EmailAddressChanged> get copyWith;
}

abstract class $PasswordChangedCopyWith<$Res> {
  factory $PasswordChangedCopyWith(
          PasswordChanged value, $Res Function(PasswordChanged) then) =
      _$PasswordChangedCopyWithImpl<$Res>;
  $Res call({String passwordString});
}

class _$PasswordChangedCopyWithImpl<$Res>
    extends _$LoginFormEventCopyWithImpl<$Res>
    implements $PasswordChangedCopyWith<$Res> {
  _$PasswordChangedCopyWithImpl(
      PasswordChanged _value, $Res Function(PasswordChanged) _then)
      : super(_value, (v) => _then(v as PasswordChanged));

  @override
  PasswordChanged get _value => super._value as PasswordChanged;

  @override
  $Res call({
    Object passwordString = freezed,
  }) {
    return _then(PasswordChanged(
      passwordString: passwordString == freezed
          ? _value.passwordString
          : passwordString as String,
    ));
  }
}

class _$PasswordChanged implements PasswordChanged {
  const _$PasswordChanged({@required this.passwordString})
      : assert(passwordString != null);

  @override
  final String passwordString;

  @override
  String toString() {
    return 'LoginFormEvent.passwordChanged(passwordString: $passwordString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is PasswordChanged &&
            (identical(other.passwordString, passwordString) ||
                const DeepCollectionEquality()
                    .equals(other.passwordString, passwordString)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(passwordString);

  @override
  $PasswordChangedCopyWith<PasswordChanged> get copyWith =>
      _$PasswordChangedCopyWithImpl<PasswordChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result emailAddressChanged(String emailAddressString),
    @required Result passwordChanged(String passwordString),
    @required Result registerWithEmailAndPasswordPressed(),
    @required Result loginWithEmailAndPasswordPressed(),
    @required Result loginWithGooglePressed(),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return passwordChanged(passwordString);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result emailAddressChanged(String emailAddressString),
    Result passwordChanged(String passwordString),
    Result registerWithEmailAndPasswordPressed(),
    Result loginWithEmailAndPasswordPressed(),
    Result loginWithGooglePressed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (passwordChanged != null) {
      return passwordChanged(passwordString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result emailAddressChanged(EmailAddressChanged value),
    @required Result passwordChanged(PasswordChanged value),
    @required
        Result registerWithEmailAndPasswordPressed(
            RegisterWithEmailAndPasswordPressed value),
    @required
        Result loginWithEmailAndPasswordPressed(
            LoginWithEmailAndPasswordPressed value),
    @required Result loginWithGooglePressed(LoginWithGooglePressed value),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return passwordChanged(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result emailAddressChanged(EmailAddressChanged value),
    Result passwordChanged(PasswordChanged value),
    Result registerWithEmailAndPasswordPressed(
        RegisterWithEmailAndPasswordPressed value),
    Result loginWithEmailAndPasswordPressed(
        LoginWithEmailAndPasswordPressed value),
    Result loginWithGooglePressed(LoginWithGooglePressed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (passwordChanged != null) {
      return passwordChanged(this);
    }
    return orElse();
  }
}

abstract class PasswordChanged implements LoginFormEvent {
  const factory PasswordChanged({@required String passwordString}) =
      _$PasswordChanged;

  String get passwordString;
  $PasswordChangedCopyWith<PasswordChanged> get copyWith;
}

abstract class $RegisterWithEmailAndPasswordPressedCopyWith<$Res> {
  factory $RegisterWithEmailAndPasswordPressedCopyWith(
          RegisterWithEmailAndPasswordPressed value,
          $Res Function(RegisterWithEmailAndPasswordPressed) then) =
      _$RegisterWithEmailAndPasswordPressedCopyWithImpl<$Res>;
}

class _$RegisterWithEmailAndPasswordPressedCopyWithImpl<$Res>
    extends _$LoginFormEventCopyWithImpl<$Res>
    implements $RegisterWithEmailAndPasswordPressedCopyWith<$Res> {
  _$RegisterWithEmailAndPasswordPressedCopyWithImpl(
      RegisterWithEmailAndPasswordPressed _value,
      $Res Function(RegisterWithEmailAndPasswordPressed) _then)
      : super(_value, (v) => _then(v as RegisterWithEmailAndPasswordPressed));

  @override
  RegisterWithEmailAndPasswordPressed get _value =>
      super._value as RegisterWithEmailAndPasswordPressed;
}

class _$RegisterWithEmailAndPasswordPressed
    implements RegisterWithEmailAndPasswordPressed {
  const _$RegisterWithEmailAndPasswordPressed();

  @override
  String toString() {
    return 'LoginFormEvent.registerWithEmailAndPasswordPressed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RegisterWithEmailAndPasswordPressed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result emailAddressChanged(String emailAddressString),
    @required Result passwordChanged(String passwordString),
    @required Result registerWithEmailAndPasswordPressed(),
    @required Result loginWithEmailAndPasswordPressed(),
    @required Result loginWithGooglePressed(),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return registerWithEmailAndPasswordPressed();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result emailAddressChanged(String emailAddressString),
    Result passwordChanged(String passwordString),
    Result registerWithEmailAndPasswordPressed(),
    Result loginWithEmailAndPasswordPressed(),
    Result loginWithGooglePressed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (registerWithEmailAndPasswordPressed != null) {
      return registerWithEmailAndPasswordPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result emailAddressChanged(EmailAddressChanged value),
    @required Result passwordChanged(PasswordChanged value),
    @required
        Result registerWithEmailAndPasswordPressed(
            RegisterWithEmailAndPasswordPressed value),
    @required
        Result loginWithEmailAndPasswordPressed(
            LoginWithEmailAndPasswordPressed value),
    @required Result loginWithGooglePressed(LoginWithGooglePressed value),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return registerWithEmailAndPasswordPressed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result emailAddressChanged(EmailAddressChanged value),
    Result passwordChanged(PasswordChanged value),
    Result registerWithEmailAndPasswordPressed(
        RegisterWithEmailAndPasswordPressed value),
    Result loginWithEmailAndPasswordPressed(
        LoginWithEmailAndPasswordPressed value),
    Result loginWithGooglePressed(LoginWithGooglePressed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (registerWithEmailAndPasswordPressed != null) {
      return registerWithEmailAndPasswordPressed(this);
    }
    return orElse();
  }
}

abstract class RegisterWithEmailAndPasswordPressed implements LoginFormEvent {
  const factory RegisterWithEmailAndPasswordPressed() =
      _$RegisterWithEmailAndPasswordPressed;
}

abstract class $LoginWithEmailAndPasswordPressedCopyWith<$Res> {
  factory $LoginWithEmailAndPasswordPressedCopyWith(
          LoginWithEmailAndPasswordPressed value,
          $Res Function(LoginWithEmailAndPasswordPressed) then) =
      _$LoginWithEmailAndPasswordPressedCopyWithImpl<$Res>;
}

class _$LoginWithEmailAndPasswordPressedCopyWithImpl<$Res>
    extends _$LoginFormEventCopyWithImpl<$Res>
    implements $LoginWithEmailAndPasswordPressedCopyWith<$Res> {
  _$LoginWithEmailAndPasswordPressedCopyWithImpl(
      LoginWithEmailAndPasswordPressed _value,
      $Res Function(LoginWithEmailAndPasswordPressed) _then)
      : super(_value, (v) => _then(v as LoginWithEmailAndPasswordPressed));

  @override
  LoginWithEmailAndPasswordPressed get _value =>
      super._value as LoginWithEmailAndPasswordPressed;
}

class _$LoginWithEmailAndPasswordPressed
    implements LoginWithEmailAndPasswordPressed {
  const _$LoginWithEmailAndPasswordPressed();

  @override
  String toString() {
    return 'LoginFormEvent.loginWithEmailAndPasswordPressed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginWithEmailAndPasswordPressed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result emailAddressChanged(String emailAddressString),
    @required Result passwordChanged(String passwordString),
    @required Result registerWithEmailAndPasswordPressed(),
    @required Result loginWithEmailAndPasswordPressed(),
    @required Result loginWithGooglePressed(),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return loginWithEmailAndPasswordPressed();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result emailAddressChanged(String emailAddressString),
    Result passwordChanged(String passwordString),
    Result registerWithEmailAndPasswordPressed(),
    Result loginWithEmailAndPasswordPressed(),
    Result loginWithGooglePressed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginWithEmailAndPasswordPressed != null) {
      return loginWithEmailAndPasswordPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result emailAddressChanged(EmailAddressChanged value),
    @required Result passwordChanged(PasswordChanged value),
    @required
        Result registerWithEmailAndPasswordPressed(
            RegisterWithEmailAndPasswordPressed value),
    @required
        Result loginWithEmailAndPasswordPressed(
            LoginWithEmailAndPasswordPressed value),
    @required Result loginWithGooglePressed(LoginWithGooglePressed value),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return loginWithEmailAndPasswordPressed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result emailAddressChanged(EmailAddressChanged value),
    Result passwordChanged(PasswordChanged value),
    Result registerWithEmailAndPasswordPressed(
        RegisterWithEmailAndPasswordPressed value),
    Result loginWithEmailAndPasswordPressed(
        LoginWithEmailAndPasswordPressed value),
    Result loginWithGooglePressed(LoginWithGooglePressed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginWithEmailAndPasswordPressed != null) {
      return loginWithEmailAndPasswordPressed(this);
    }
    return orElse();
  }
}

abstract class LoginWithEmailAndPasswordPressed implements LoginFormEvent {
  const factory LoginWithEmailAndPasswordPressed() =
      _$LoginWithEmailAndPasswordPressed;
}

abstract class $LoginWithGooglePressedCopyWith<$Res> {
  factory $LoginWithGooglePressedCopyWith(LoginWithGooglePressed value,
          $Res Function(LoginWithGooglePressed) then) =
      _$LoginWithGooglePressedCopyWithImpl<$Res>;
}

class _$LoginWithGooglePressedCopyWithImpl<$Res>
    extends _$LoginFormEventCopyWithImpl<$Res>
    implements $LoginWithGooglePressedCopyWith<$Res> {
  _$LoginWithGooglePressedCopyWithImpl(LoginWithGooglePressed _value,
      $Res Function(LoginWithGooglePressed) _then)
      : super(_value, (v) => _then(v as LoginWithGooglePressed));

  @override
  LoginWithGooglePressed get _value => super._value as LoginWithGooglePressed;
}

class _$LoginWithGooglePressed implements LoginWithGooglePressed {
  const _$LoginWithGooglePressed();

  @override
  String toString() {
    return 'LoginFormEvent.loginWithGooglePressed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoginWithGooglePressed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result emailAddressChanged(String emailAddressString),
    @required Result passwordChanged(String passwordString),
    @required Result registerWithEmailAndPasswordPressed(),
    @required Result loginWithEmailAndPasswordPressed(),
    @required Result loginWithGooglePressed(),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return loginWithGooglePressed();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result emailAddressChanged(String emailAddressString),
    Result passwordChanged(String passwordString),
    Result registerWithEmailAndPasswordPressed(),
    Result loginWithEmailAndPasswordPressed(),
    Result loginWithGooglePressed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginWithGooglePressed != null) {
      return loginWithGooglePressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result emailAddressChanged(EmailAddressChanged value),
    @required Result passwordChanged(PasswordChanged value),
    @required
        Result registerWithEmailAndPasswordPressed(
            RegisterWithEmailAndPasswordPressed value),
    @required
        Result loginWithEmailAndPasswordPressed(
            LoginWithEmailAndPasswordPressed value),
    @required Result loginWithGooglePressed(LoginWithGooglePressed value),
  }) {
    assert(emailAddressChanged != null);
    assert(passwordChanged != null);
    assert(registerWithEmailAndPasswordPressed != null);
    assert(loginWithEmailAndPasswordPressed != null);
    assert(loginWithGooglePressed != null);
    return loginWithGooglePressed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result emailAddressChanged(EmailAddressChanged value),
    Result passwordChanged(PasswordChanged value),
    Result registerWithEmailAndPasswordPressed(
        RegisterWithEmailAndPasswordPressed value),
    Result loginWithEmailAndPasswordPressed(
        LoginWithEmailAndPasswordPressed value),
    Result loginWithGooglePressed(LoginWithGooglePressed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loginWithGooglePressed != null) {
      return loginWithGooglePressed(this);
    }
    return orElse();
  }
}

abstract class LoginWithGooglePressed implements LoginFormEvent {
  const factory LoginWithGooglePressed() = _$LoginWithGooglePressed;
}

class _$LoginFormStateTearOff {
  const _$LoginFormStateTearOff();

// ignore: unused_element
  _LoginFormState call(
      {@required
          EmailAddress emailAddress,
      @required
          Password password,
      @required
          bool showEmailAddressError,
      @required
          bool showPasswordError,
      @required
          bool isSubmitting,
      @required
          Option<Either<AbstractAuthenticationFailures, Unit>>
              authenticationStatus}) {
    return _LoginFormState(
      emailAddress: emailAddress,
      password: password,
      showEmailAddressError: showEmailAddressError,
      showPasswordError: showPasswordError,
      isSubmitting: isSubmitting,
      authenticationStatus: authenticationStatus,
    );
  }
}

// ignore: unused_element
const $LoginFormState = _$LoginFormStateTearOff();

mixin _$LoginFormState {
  EmailAddress get emailAddress;
  Password get password;
  bool get showEmailAddressError;
  bool get showPasswordError;
  bool get isSubmitting;
  Option<Either<AbstractAuthenticationFailures, Unit>> get authenticationStatus;

  $LoginFormStateCopyWith<LoginFormState> get copyWith;
}

abstract class $LoginFormStateCopyWith<$Res> {
  factory $LoginFormStateCopyWith(
          LoginFormState value, $Res Function(LoginFormState) then) =
      _$LoginFormStateCopyWithImpl<$Res>;
  $Res call(
      {EmailAddress emailAddress,
      Password password,
      bool showEmailAddressError,
      bool showPasswordError,
      bool isSubmitting,
      Option<Either<AbstractAuthenticationFailures, Unit>>
          authenticationStatus});
}

class _$LoginFormStateCopyWithImpl<$Res>
    implements $LoginFormStateCopyWith<$Res> {
  _$LoginFormStateCopyWithImpl(this._value, this._then);

  final LoginFormState _value;
  // ignore: unused_field
  final $Res Function(LoginFormState) _then;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object password = freezed,
    Object showEmailAddressError = freezed,
    Object showPasswordError = freezed,
    Object isSubmitting = freezed,
    Object authenticationStatus = freezed,
  }) {
    return _then(_value.copyWith(
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      password: password == freezed ? _value.password : password as Password,
      showEmailAddressError: showEmailAddressError == freezed
          ? _value.showEmailAddressError
          : showEmailAddressError as bool,
      showPasswordError: showPasswordError == freezed
          ? _value.showPasswordError
          : showPasswordError as bool,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      authenticationStatus: authenticationStatus == freezed
          ? _value.authenticationStatus
          : authenticationStatus
              as Option<Either<AbstractAuthenticationFailures, Unit>>,
    ));
  }
}

abstract class _$LoginFormStateCopyWith<$Res>
    implements $LoginFormStateCopyWith<$Res> {
  factory _$LoginFormStateCopyWith(
          _LoginFormState value, $Res Function(_LoginFormState) then) =
      __$LoginFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {EmailAddress emailAddress,
      Password password,
      bool showEmailAddressError,
      bool showPasswordError,
      bool isSubmitting,
      Option<Either<AbstractAuthenticationFailures, Unit>>
          authenticationStatus});
}

class __$LoginFormStateCopyWithImpl<$Res>
    extends _$LoginFormStateCopyWithImpl<$Res>
    implements _$LoginFormStateCopyWith<$Res> {
  __$LoginFormStateCopyWithImpl(
      _LoginFormState _value, $Res Function(_LoginFormState) _then)
      : super(_value, (v) => _then(v as _LoginFormState));

  @override
  _LoginFormState get _value => super._value as _LoginFormState;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object password = freezed,
    Object showEmailAddressError = freezed,
    Object showPasswordError = freezed,
    Object isSubmitting = freezed,
    Object authenticationStatus = freezed,
  }) {
    return _then(_LoginFormState(
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      password: password == freezed ? _value.password : password as Password,
      showEmailAddressError: showEmailAddressError == freezed
          ? _value.showEmailAddressError
          : showEmailAddressError as bool,
      showPasswordError: showPasswordError == freezed
          ? _value.showPasswordError
          : showPasswordError as bool,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      authenticationStatus: authenticationStatus == freezed
          ? _value.authenticationStatus
          : authenticationStatus
              as Option<Either<AbstractAuthenticationFailures, Unit>>,
    ));
  }
}

class _$_LoginFormState implements _LoginFormState {
  const _$_LoginFormState(
      {@required this.emailAddress,
      @required this.password,
      @required this.showEmailAddressError,
      @required this.showPasswordError,
      @required this.isSubmitting,
      @required this.authenticationStatus})
      : assert(emailAddress != null),
        assert(password != null),
        assert(showEmailAddressError != null),
        assert(showPasswordError != null),
        assert(isSubmitting != null),
        assert(authenticationStatus != null);

  @override
  final EmailAddress emailAddress;
  @override
  final Password password;
  @override
  final bool showEmailAddressError;
  @override
  final bool showPasswordError;
  @override
  final bool isSubmitting;
  @override
  final Option<Either<AbstractAuthenticationFailures, Unit>>
      authenticationStatus;

  @override
  String toString() {
    return 'LoginFormState(emailAddress: $emailAddress, password: $password, showEmailAddressError: $showEmailAddressError, showPasswordError: $showPasswordError, isSubmitting: $isSubmitting, authenticationStatus: $authenticationStatus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoginFormState &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.showEmailAddressError, showEmailAddressError) ||
                const DeepCollectionEquality().equals(
                    other.showEmailAddressError, showEmailAddressError)) &&
            (identical(other.showPasswordError, showPasswordError) ||
                const DeepCollectionEquality()
                    .equals(other.showPasswordError, showPasswordError)) &&
            (identical(other.isSubmitting, isSubmitting) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitting, isSubmitting)) &&
            (identical(other.authenticationStatus, authenticationStatus) ||
                const DeepCollectionEquality()
                    .equals(other.authenticationStatus, authenticationStatus)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(showEmailAddressError) ^
      const DeepCollectionEquality().hash(showPasswordError) ^
      const DeepCollectionEquality().hash(isSubmitting) ^
      const DeepCollectionEquality().hash(authenticationStatus);

  @override
  _$LoginFormStateCopyWith<_LoginFormState> get copyWith =>
      __$LoginFormStateCopyWithImpl<_LoginFormState>(this, _$identity);
}

abstract class _LoginFormState implements LoginFormState {
  const factory _LoginFormState(
      {@required
          EmailAddress emailAddress,
      @required
          Password password,
      @required
          bool showEmailAddressError,
      @required
          bool showPasswordError,
      @required
          bool isSubmitting,
      @required
          Option<Either<AbstractAuthenticationFailures, Unit>>
              authenticationStatus}) = _$_LoginFormState;

  @override
  EmailAddress get emailAddress;
  @override
  Password get password;
  @override
  bool get showEmailAddressError;
  @override
  bool get showPasswordError;
  @override
  bool get isSubmitting;
  @override
  Option<Either<AbstractAuthenticationFailures, Unit>> get authenticationStatus;
  @override
  _$LoginFormStateCopyWith<_LoginFormState> get copyWith;
}
