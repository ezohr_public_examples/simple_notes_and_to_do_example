part of 'login_form_bloc.dart';

// @immutable
// abstract class LoginFormState {}

// class LoginFormInitial extends LoginFormState {}

// * Direction: To user interface
@immutable
@freezed
abstract class LoginFormState with _$LoginFormState {
  const factory LoginFormState(
      {@required
          EmailAddress emailAddress,
      @required
          Password password,
      @required
          bool showEmailAddressError,
      @required
          bool showPasswordError,
      @required
          bool isSubmitting,
      @required
          Option<Either<AbstractAuthenticationFailures, Unit>>
              authenticationStatus}) = _LoginFormState;

  factory LoginFormState.initial() => LoginFormState(
      emailAddress: EmailAddress(emailAddressString: ''),
      password: Password(passwordString: ''),
      showEmailAddressError: false,
      showPasswordError: false,
      isSubmitting: false,
      authenticationStatus: none());
}
