import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/abstracts/abstract_authentication_failures.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/interfaces/interface_authentication_facade.dart';
import 'package:simple_notes_and_to_do_example/4_enterprise_business_rules/entities/types/types.dart';

part 'login_form_bloc.freezed.dart';
part 'login_form_event.dart';
part 'login_form_state.dart';

class LoginFormBloc extends Bloc<LoginFormEvent, LoginFormState> {
  final InterfaceAuthenticationFacade _interfaceAuthenticationFacade;

  // LoginFormBloc() : super(LoginFormInitial());

  LoginFormBloc(this._interfaceAuthenticationFacade);

  @override
  LoginFormState get initialState => LoginFormState.initial();

  @override
  Stream<LoginFormState> mapEventToState(
    LoginFormEvent event,
  ) async* {
    // TO DO: implement mapEventToState
    yield* event.map(emailAddressChanged: (error) async* {
      yield state.copyWith(
          emailAddress:
              EmailAddress(emailAddressString: error.emailAddressString),
          showEmailAddressError: true,
          authenticationStatus: none());
    }, passwordChanged: (error) async* {
      yield state.copyWith(
          password: Password(passwordString: error.passwordString),
          showPasswordError: true,
          authenticationStatus: none());
    }, registerWithEmailAndPasswordPressed: (error) async* {
      yield* _registerOrLoginWithEmailAndPassword(
          _interfaceAuthenticationFacade.registerWithEmailAndPassword);
    }, loginWithEmailAndPasswordPressed: (error) async* {
      yield* _registerOrLoginWithEmailAndPassword(
          _interfaceAuthenticationFacade.loginWithEmailAndPassword);
    }, loginWithGooglePressed: (error) async* {
      yield state.copyWith(isSubmitting: true, authenticationStatus: none());

      final Either<AbstractAuthenticationFailures, Unit> loggedIn =
          await _interfaceAuthenticationFacade.loginWithGoogle();

      yield state.copyWith(
          isSubmitting: false, authenticationStatus: some(loggedIn));
    });
  }

  Stream<LoginFormState> _registerOrLoginWithEmailAndPassword(
      Future<Either<AbstractAuthenticationFailures, Unit>> Function(
              {@required EmailAddress emailAddress,
              @required Password password})
          registerOrLoginFunction) async* {
    if (state.emailAddress.isValid() && state.password.isValid()) {
      yield state.copyWith(isSubmitting: true, authenticationStatus: none());

      final Either<AbstractAuthenticationFailures, Unit> loggedIn =
          await registerOrLoginFunction(
              emailAddress: state.emailAddress, password: state.password);

      yield state.copyWith(
          isSubmitting: false, authenticationStatus: some(loggedIn));
    } else {
      // Alternative: If loggedIn is declared outside of the if statment, then we can use authenticationStatus: optionOf(loggedIn) instead of the below
      yield state.copyWith(authenticationStatus: none());
    }
  }
}
